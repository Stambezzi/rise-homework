namespace HashTasksTests
{
    using HashTasks;
    using System.Reflection.Metadata;

    [TestClass]
    public class HashTasksTests
    {
        [TestMethod]
        public void GetCarOwner_Returns_Owner_Name()
        {
            //Arrange
            var input = "PK0734BC";

            var carsAndOwners = new Dictionary<string, string>();
            carsAndOwners.Add("PK0734BC", "David");
            carsAndOwners.Add("CB0744BC", "Petko");

            var expected = "David";

            //Act
            var result = HashTasks.GetCarOwner(carsAndOwners, input);

            //Assert
            Assert.AreEqual(expected, result[0]);
        }

        [TestMethod]
        public void GetCarOwner_Returns_Owner_Name_And_Owners_Of_Multiple_Cars()
        {
            //Arrange
            var input = "PK0734BC";

            var carsAndOwners = new Dictionary<string, string>();
            carsAndOwners.Add("PK0734BC", "David");
            carsAndOwners.Add("CB0744BC", "Petko");
            carsAndOwners.Add("PK0357BC", "David");

            var expected = new List<string> { "David", "Owners with more than one car: ", "David" };

            //Act
            var result = HashTasks.GetCarOwner(carsAndOwners, input);

            //Assert
            CollectionAssert.AreEqual(expected, result);
        }

        [TestMethod]
        public void GetCarOwner_Throws_Exception_If_Plate_Is_Not_Owned()
        {
            //Arrange
            var input = "test";

            var carsAndOwners = new Dictionary<string, string>();
            carsAndOwners.Add("PK0734BC", "David");
            carsAndOwners.Add("CB0744BC", "Petko");

            //Act
            //Assert
            Assert.ThrowsException<Exception>(() => HashTasks.GetCarOwner(carsAndOwners, input));
        }

        [TestMethod]
        public void GetIntersectingElements_Returns_With_One_Intersecting_Element()
        {
            //Arrange
            var firstArr = new string[] { "a", "b", "c" };
            var secondArr = new string[] { "a" };

            var expected = new string[] { "a" };

            //Act
            var result = HashTasks.GetIntersectingElements(firstArr, secondArr);

            //Assert
            CollectionAssert.AreEquivalent(expected, result);
        }

        [TestMethod]
        public void GetIntersectingElements_Returns_With_Three_Intersecting_Element()
        {
            //Arrange
            var firstArr = new string[] { "a", "b", "c", "test", "d" };
            var secondArr = new string[] { "a", "b", "test" };

            var expected = new string[] { "a", "b", "test" };

            //Act
            var result = HashTasks.GetIntersectingElements(firstArr, secondArr);

            //Assert
            CollectionAssert.AreEquivalent(expected, result);
        }

        [TestMethod]
        public void GetIntersectingElements_Returns_With_No_Intersecting_Element()
        {
            //Arrange
            var firstArr = new string[] { "a", "b", "c" };
            var secondArr = new string[] { "z", "x", "y" };

            var expected = new string[] { };

            //Act
            var result = HashTasks.GetIntersectingElements(firstArr, secondArr);

            //Assert
            CollectionAssert.AreEquivalent(expected, result);
        }

        [TestMethod]
        public void GetAllNonRepeatingCharactersAndFirstNonRepeatingCharacterIndex_Of_String_With_One_Non_Repeating_Char()
        {
            //Arrange
            var input = "abbcc";

            var expected = new List<string> { "a", "Index of first non-repeating character: 0" };

            //Act
            var result = HashTasks.GetAllNonRepeatingCharactersAndFirstNonRepeatingCharacterIndex(input);

            //Assert
            CollectionAssert.AreEquivalent(expected, result);
        }

        [TestMethod]
        public void GetAllNonRepeatingCharactersAndFirstNonRepeatingCharacterIndex_Of_String_With_Three_Non_Repeating_Char()
        {
            //Arrange
            var input = "aabcddf";

            var expected = new List<string> { "b, c, f", "Index of first non-repeating character: 2" };

            //Act
            var result = HashTasks.GetAllNonRepeatingCharactersAndFirstNonRepeatingCharacterIndex(input);

            //Assert
            CollectionAssert.AreEquivalent(expected, result);
        }

        [TestMethod]
        public void GetAllNonRepeatingCharactersAndFirstNonRepeatingCharacterIndex_Of_String_With_No_Non_Repeating_Char()
        {
            //Arrange
            var input = "aabb";

            var expected = new List<string> { "", "Index of first non-repeating character: -1" };

            //Act
            var result = HashTasks.GetAllNonRepeatingCharactersAndFirstNonRepeatingCharacterIndex(input);

            //Assert
            CollectionAssert.AreEquivalent(expected, result);
        }

        [TestMethod]
        public void GetAllNonRepeatingCharactersAndFirstNonRepeatingCharacterIndex_Of_Empty_String()
        {
            //Arrange
            var input = "";

            var expected = new List<string> { "", "Index of first non-repeating character: -1" };

            //Act
            var result = HashTasks.GetAllNonRepeatingCharactersAndFirstNonRepeatingCharacterIndex(input);

            //Assert
            CollectionAssert.AreEquivalent(expected, result);
        }

        [TestMethod]
        public void GetMisspelledWords_With_One_Misspelled_Word()
        {
            //Arrange
            var input = "Hello, this word is misssspelt and this one is not.";

            var set = new HashSet<string> { "Hello", "this", "word", "is", "misspelled", "and", "this", "one", "not" };

            var expected = new List<string> { "misssspelt" };

            //Act
            var result = HashTasks.GetMisspelledWords(input, set);

            //Assert
            CollectionAssert.AreEquivalent(expected, result);
        }

        [TestMethod]
        public void GetMisspelledWords_With_Three_Misspelled_Word()
        {
            //Arrange
            var input = "Hello, this wrod ist misssspelt and this one is not.";

            var set = new HashSet<string> { "Hello", "this", "word", "is", "misspelled", "and", "this", "one", "not" };

            var expected = new List<string> { "wrod", "ist", "misssspelt" };

            //Act
            var result = HashTasks.GetMisspelledWords(input, set);

            //Assert
            CollectionAssert.AreEquivalent(expected, result);
        }

        [TestMethod]
        public void GetMisspelledWords_With_No_Misspelled_Word()
        {
            //Arrange
            var input = "Hello, this word is misspelled and this one is not.";

            var set = new HashSet<string> { "Hello", "this", "word", "is", "misspelled", "and", "this", "one", "not" };

            var expected = new List<string> { };

            //Act
            var result = HashTasks.GetMisspelledWords(input, set);

            //Assert
            CollectionAssert.AreEquivalent(expected, result);
        }


        [TestMethod]
        public void GetMisspelledWords_From_Empty_String()
        {
            //Arrange
            var input = "";

            var set = new HashSet<string> { "Hello", "this", "word", "is", "misspelled", "and", "this", "one", "not" };

            var expected = new List<string> { };

            //Act
            var result = HashTasks.GetMisspelledWords(input, set);

            //Assert
            CollectionAssert.AreEquivalent(expected, result);
        }

        [TestMethod]
        public void GroupWordsByAnagrams_With_One_Group()
        {
            //Arrange
            var input = new List<string> { "rat", "art", "tar", "rat" };
            var sorted = input.Select(x => string.Join("", x.OrderBy(y => y))).ToArray();

            var expected = new Dictionary<string, List<string>> { { "rat", new List<string> { "rat", "art", "tar" } } };

            //Act
            var result = HashTasks.GroupWordsByAnagrams(input, sorted);

            //Assert
            CollectionAssert.AreEquivalent(expected["rat"], result["rat"]);
        }

        [TestMethod]
        public void GroupWordsByAnagrams_With_Two_Group()
        {
            //Arrange
            var input = new List<string> { "rat", "art", "tab", "bat" };
            var sorted = input.Select(x => string.Join("", x.OrderBy(y => y))).ToArray();

            var expected = new Dictionary<string, List<string>> { { "rat", new List<string> { "rat", "art" } }, { "tab", new List<string> { "tab", "bat" } } };

            //Act
            var result = HashTasks.GroupWordsByAnagrams(input, sorted);

            //Assert
            CollectionAssert.AreEquivalent(expected["rat"], result["rat"]);
            CollectionAssert.AreEquivalent(expected["tab"], result["tab"]);
        }

        [TestMethod]
        public void GroupWordsByAnagrams_With_No_Groups()
        {
            //Arrange
            var input = new List<string> { "rat", "tab" };
            var sorted = input.Select(x => string.Join("", x.OrderBy(y => y))).ToArray();

            var expected = new Dictionary<string, List<string>> { { "rat", new List<string> { "rat" } }, { "tab", new List<string> { "tab" } } };

            //Act
            var result = HashTasks.GroupWordsByAnagrams(input, sorted);

            //Assert
            CollectionAssert.AreEquivalent(expected["rat"], result["rat"]);
            CollectionAssert.AreEquivalent(expected["tab"], result["tab"]);
        }

        [TestMethod]
        public void CreateUser_Works_And_Hashes_Password()
        {
            //Arrange
            var userName = "Test";
            var password = "123";

            var expected = new HashTasks.User { UserName = userName, Password = "A665A45920422F9D417E4867EFDC4FB8A04A1F3FFF1FA07E998E86F7F7A27AE3" };

            //Act
            var result = HashTasks.CreateUser(userName, "123");

            //Assert
            Assert.AreEqual(expected.UserName, result.UserName);
            Assert.AreEqual(expected.Password, result.Password);
        }
    }
}