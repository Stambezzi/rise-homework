﻿namespace HashTasks
{
    using System.Linq;

    using System.Security.Cryptography;
    using System.Text;

    public class HashTasks
    {
        public class User
        {
            public string UserName { get; set; }

            public string Password { get; set; }

        }

        public static void Main()
        {
            CreateUser("Test", "123");
        }

        public static User CreateUser(string username, string password)
        {
            var user = new User();

            user.UserName = username;
            // Супер е както си направил хеширането, но само връщаш юзър, без да добавяш в таблица
            user.Password = HashPassword(password);

            return user;
        }

        public static string HashPassword(string secret)
        {
            using var sha256 = SHA256.Create();

            var secretBytes = Encoding.UTF8.GetBytes(secret);
            var secretHash = sha256.ComputeHash(secretBytes);

            return Convert.ToHexString(secretHash);
        }

        public static Dictionary<string, List<string>> GroupWordsByAnagrams(List<string> input, string[] sorted)
        {
            // Избягвай да пишеш var навсякъде, особено когато имаш хубави типове, кода става доста трудно четим така
            var result = new Dictionary<string, List<string>>();

            for (int i = 0; i < input.Count; i++)
            {
                //тук например споккойно можеш да напишеш "string" word
                var word = input[i];
                var sortedWord = string.Join("", sorted[i]);

                if (result.Keys.Any(x => sortedWord == string.Join("", x.OrderBy(y => y))))
                {
                    // иииии избягавай continue, не е добра практика
                    continue;
                }

                if (!result.ContainsKey(word))
                {
                    result[word] = new List<string> { word };
                }

                for (int j = i + 1; j < input.Count; j++)
                {
                    var searchWord = string.Join("", sorted[j]);

                    if (searchWord == sortedWord)
                    {
                        if (result[word].Contains(input[j]))
                        {
                            //мисля че имаш грешка тук
                            continue;
                        }

                        //като цяло тази задача си я разбъркал доста, има много по-елегантно решение, може да го обсъдим утре ако искаш
                        result[word].Add(input[j]);
                    }
                }
            }

            return result;
        }

        public static List<string> GetMisspelledWords(string input, HashSet<string> set)
        {
            //пак коментара с var-овете
            var result = new List<string>();

            var inputWithoutPunctuation = input.Split(" ,-!.".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            foreach (var word in inputWithoutPunctuation)
            {
                if (set.Contains(word) == false)
                {
                    result.Add(word);
                }
            }

            return result;
        }

        public static List<string> GetAllNonRepeatingCharactersAndFirstNonRepeatingCharacterIndex(string input)
        {
            // пак ще те тормозя с var-овете не е javascript това 
            var result = new List<string>();
            var dict = new Dictionary<char, int>();

            for (int i = 0; i < input.Length; i++)
            {
                if (!dict.ContainsKey(input[i]))
                {
                    dict.Add(input[i], 1);
                }
                else
                {
                    dict[input[i]]++;
                }
            }

            var nonRepeating = dict.Where(x => x.Value == 1).Select(x => x.Key);

            // защо в масив със стрингове добавяш един голям стринг който има всички елементи?
            result.Add(string.Join(", ", nonRepeating));

            if (nonRepeating.Any())
            {
                var firstChar = nonRepeating.First();

                result.Add("Index of first non-repeating character: " + input.IndexOf(firstChar));

                //мултипъл ритърните са лоша практика, можеш да ползваш else вместо това.
                return result;
            }

            result.Add("Index of first non-repeating character: " + "-1");

            //в един масив добавяш 2 неща, които са тотално различни, прочети условието на задачата.
            // като цяло си я направил, но резултата е много гадно върнат
            return result;
        }

        public static string[] GetIntersectingElements(string[] firstArray, string[] secondArray)
        {
            //var
            //защо копираш двата масива в стринг??
            var combinedArrays = new string[firstArray.Length + secondArray.Length];
            firstArray.CopyTo(combinedArrays, 0);
            secondArray.CopyTo(combinedArrays, firstArray.Length);

            var set = new HashSet<string>();

            var result = new List<string>();

            foreach (var element in combinedArrays)
            {
                //тази проверка е излизашна, може просто да добавяш, ако елемента го има, то няма да се добави.
                if (set.Add(element) == false)
                {
                    result.Add(element);
                }
            }

            return result.ToArray();
        }

        public static List<string> GetCarOwner(Dictionary<string, string> dict, string plateNumber)
        {
            var result = new List<string>();

            if (dict.ContainsKey(plateNumber) == false)
            {
                throw new Exception("This plate doesn't have an owner!");
            }

            var ownerName = dict[plateNumber];
            result.Add(ownerName);

            var allNames = dict.Select(x => x.Value).ToList();
            var namesWithMoreThanOneCar = allNames.GroupBy(x => x).Where(g => g.Count() > 1).Select(x => x.Key);


            //отново тъй като са 2 под точки бих очаквал 2 функции, това не ми харесва като изход. 
            if (namesWithMoreThanOneCar.Any())
            {
                result.Add("Owners with more than one car: ");
                result.Add(string.Join(", ", namesWithMoreThanOneCar));
            }

            return result;
        }
    }
}

//Направил си голяма част от нещата, но според мен не можеш да нацелиш все още в коя ситуация какъв тип данни да ползваш
//Да не би да си писал на javascript или python преди? 
//Имаш ли нужда да ги изговорим тези неща?