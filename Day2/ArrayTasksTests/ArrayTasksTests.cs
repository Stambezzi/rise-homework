namespace ArrayTasksTests
{
    using ArrayTasks;

    [TestClass]
    public class ArrayTasksTests
    {
        [TestMethod]
        public void Correctly_Returns_Missing_Number() 
        {
            //Arrange
            var input = new int[] { 3, 7, 8, 1, 9, 10, 2, 4, 6 };
            var expected = 5;

            //Act
            var result = ArrayTasks.FindMissingNumber(input);

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void Correctly_Returns_Missing_Number_In_Array_With_Length_Two()
        {
            //Arrange
            var input = new int[] { 101, 103 };
            var expected = 102;

            //Act
            var result = ArrayTasks.FindMissingNumber(input);

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void Returns_Minus_One_If_There_Is_No_Misiing_Number()
        {
            //Arrange
            var input = new int[] { 3, 7, 8, 1, 9, 10, 2, 4, 6 , 5 };
            var expected = -1;

            //Act
            var result = ArrayTasks.FindMissingNumber(input);

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void Returns_Minus_One_If_There_Is_One_Number()
        {
            //Arrange
            var input = new int[] { 3 };
            var expected = -1;

            //Act
            var result = ArrayTasks.FindMissingNumber(input);

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void Returns_Minus_One_If_There_Is_No_Numbers()
        {
            //Arrange
            var input = new int[] { };
            var expected = -1;

            //Act
            var result = ArrayTasks.FindMissingNumber(input);

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void Find_Cube_Root_Returns_Correct_Number()
        {
            //Arrange
            var input = 27;
            var expected = 3;

            //Act
            var result = ArrayTasks.FindCubeRoot(input);

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void Find_Cube_Root_Of_Number_With_Non_Int_Cube_Root()
        {
            //Arrange
            var input = 26;

            //Act
            //Assert
            Assert.ThrowsException<Exception>(() => ArrayTasks.FindCubeRoot(input));
        }
    }
}