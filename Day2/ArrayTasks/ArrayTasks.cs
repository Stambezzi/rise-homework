﻿namespace ArrayTasks
{
    public class ArrayTasks
    {
        public static void Main()
        {
            var input = 27;

            var result = FindCubeRoot(input);

            Console.WriteLine(result);
        }

        public static int FindCubeRoot(int number)
        {
            var minCounter = 0;
            int maxCounter = 1920;

            var midCounter = 0;
            var midCube = 0;

            while (minCounter <= maxCounter)
            {
                midCounter = (minCounter + maxCounter) / 2;
                midCube = midCounter * midCounter * midCounter;

                if (midCube == number)
                {
                    return midCounter;
                }
                else if (midCube < number)
                {
                    minCounter = midCounter + 1;
                }
                else
                {
                    maxCounter = midCounter - 1;
                }
            }

            throw new Exception("Cube root isn't an integer.");
        }

        public static int FindMissingNumber(int[] arr)
        {
            var sorted = ArraySorted(arr);

            if (arr.Length == 2)
            {
                return arr[0] + 1;
            }

            for (int i = 1; i < arr.Length - 2; i++)
            {
                if (arr[i] != arr[i - 1] + 1)
                {
                    return arr[i - 1] + 1;
                }
            }

            return -1;
        }

        private static int[] ArraySorted(int[] arr)
        {
            int temp = 0;

            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = i + 1; j < arr.Length; j++)
                {
                    if (arr[i] > arr[j])
                    {
                        temp = arr[j];
                        arr[j] = arr[i];
                        arr[i] = temp;
                    }
                }
            }

            return arr;
        }
    }
}