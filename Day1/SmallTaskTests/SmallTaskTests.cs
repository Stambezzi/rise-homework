namespace SmallTaskTests
{
    using SmallTasks;

    [TestClass]
    public class SmallTaskTests
    {
        [TestMethod]
        public void MatrixContainsSquareWithFibonacciSequence_Returns_True_With_Two_Length_Square_Containing_Fibonacci_Sequence()
        {
            //Arrange
            int[,] input = new int[,]
            {
                { 1, 1, 1 },
                { 4, 3, 2 },
                { 7, 13, 8 }
            };

            var expected = true;

            //Act
            var result = SmallTasks.MatrixContainsSquareWithFibonacciSequence(input);

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void MatrixContainsSquareWithFibonacciSequence_Returns_True_With_Three_Length_Square_Containing_Fibonacci_Sequence()
        {
            //Arrange
            int[,] input = new int[,]
             {
                { 0, 1, 1, 2 },
                { 0, 21, 0, 3 },
                { 0, 13, 8, 5 },
                { 0, 0, 0, 0 },
             };

            var expected = true;

            //Act
            var result = SmallTasks.MatrixContainsSquareWithFibonacciSequence(input);

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void MatrixContainsSquareWithFibonacciSequence_Returns_False_If_No_Squares_Contain_Fibonacci_Sequence()
        {
            //Arrange
            int[,] input = new int[,]
             {
                { 1, 1, 1, 1 },
                { 1, 1, 1, 1 },
                { 1, 1, 1, 1 },
                { 1, 1, 1, 1 },
             };

            var expected = false;

            //Act
            var result = SmallTasks.MatrixContainsSquareWithFibonacciSequence(input);

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void CheckFibonacciNeighbors_Returns_True_If_All_Eight_Neighbors_Make_A_Fibonacci_Sequence()
        {
            //Arrange
            int[,] input = new int[,]
            {
                { 0, 1, 1, 2 },
                { 0, 21, 0, 3 },
                { 0, 13, 8, 5 },
                { 0, 0, 0, 0 },
            };

            var expected = true;

            //Act
            var result = SmallTasks.CheckFibonacciNeighbors(input);

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void CheckFibonacciNeighbors_Returns_True_If_Less_Than_Eight_Neighbors_Make_A_Fibonacci_Sequence()
        {
            //Arrange
            int[,] input = new int[,]
             {
                { 5, 0, 1, 0 },
                { 3, 2, 1, 0 },
                { 0, 0, 0, 0 },
                { 0, 0, 0, 0 },
            };

            var expected = true;

            //Act
            var result = SmallTasks.CheckFibonacciNeighbors(input);

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void CheckFibonacciNeighbors_Returns_False_If_No_Number_Has_Neighbors_That_Make_A_Fibonacci_Sequence()
        {
            //Arrange
            int[,] input = new int[,]
            {
                { 1, 1, 1, 1 },
                { 1, 1, 1, 1 },
                { 1, 1, 1, 1 },
                { 1, 1, 1, 1 },
            };

            var expected = false;

            //Act
            var result = SmallTasks.CheckFibonacciNeighbors(input);

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void ArrangeByBits_Correctly_Returns_Sorted_Array()
        {
            //Arrange
            int[] input = new int[] { 10, 1 };

            int[] expected = new int[] { 1, 10 }; 

            //Act
            var result = SmallTasks.ArrangeByBits(input);

            //Assert
            CollectionAssert.AreEqual(expected, result);
        }

        [TestMethod]
        public void ArrangeByBits_Correctly_Places_Which_Number_Was_First_If_Same_Bit_Sum()
        {
            //Arrange
            int[] input = new int[] { 10, 20, 1 };

            int[] expected = new int[] { 1, 10, 20 };

            //Act
            var result = SmallTasks.ArrangeByBits(input);

            //Assert
            CollectionAssert.AreEqual(expected, result);
        }
    }
}