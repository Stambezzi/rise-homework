﻿namespace SmallTasks
{
    public class SmallTasks
    {
        public static void Main()
        {
            var input = new int[] { 112, 35, 35, 20, 10 };

            var result = ArrangeByBits(input);

            Console.WriteLine(String.Join(", ", result));
        }

        public static int[] ArrangeByBits(int[] arr)
        {
            var tuples = new List<(int, int)>();

            foreach (var num in arr)
            {
                int sumOfBits = CountBits(num);
                tuples.Add((sumOfBits, num));
            }

            tuples.Sort();

            int[] result = new int[arr.Length];
            int index = 0;

            foreach (var tuple in tuples)
            {
                result[index++] = tuple.Item2;
            }

            return result;
        }

        private static int CountBits(int num)
        {
            int count = 0;

            while (num != 0)
            {
                count += num & 1;
                num >>= 1;
            }

            return count;
        }

        public static bool CheckFibonacciNeighbors(int[,] arr)
        {
            int rows = arr.GetLength(0);
            int cols = arr.GetLength(1);

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    if (HasFibonacciNeighbors(i, j, rows, cols, arr))
                    {
                        return true;
                    } 
                }
            }

            return false;
        }

        private static bool HasFibonacciNeighbors(int x, int y, int rows, int cols, int[,] arr)
        {
            int[] neighbors = new int[8];
            int index = 0;

            if (x > 0 && y > 0)
            {
                neighbors[index++] = arr[x - 1, y - 1];
            }
            if (x > 0) 
            { 
                neighbors[index++] = arr[x - 1, y]; 
            }
            if (x > 0 && y < cols - 1)
            {
                neighbors[index++] = arr[x - 1, y + 1];
            }
            if (y < cols - 1)
            {
                neighbors[index++] = arr[x, y + 1];
            }
            if (x < rows - 1 && y < cols - 1)
            {
                neighbors[index++] = arr[x + 1, y + 1];
            }
            if (x < rows - 1)
            {
                neighbors[index++] = arr[x + 1, y];
            }
            if (x < rows - 1 && y > 0)
            {
                neighbors[index++] = arr[x + 1, y - 1];
            }
            if (y > 0)
            {
                neighbors[index++] = arr[x, y - 1];
            }

            return IsFibonacciSequence(neighbors);
        }

        public static bool MatrixContainsSquareWithFibonacciSequence(int[,] matrix)
        {
            int matrixSideLength = matrix.GetLength(0);

            for (int i = 0; i < matrixSideLength - 1; i++)
            {
                for (int j = 0; j < matrixSideLength - 1; j++)
                {
                    if (matrixSideLength - i <= 1 || matrixSideLength - j <= 1)
                    {
                        continue;
                    }
                    if (IsFibonacciSquare(i, j, matrixSideLength, matrix))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private static bool IsFibonacciSquare(int x, int y, int n, int[,] arr)
        {
            int len = 2;

            while (len <= n - x && len <= n - y)
            {
                int[] border = new int[4 * len - 4];
                int index = 0;

                for (int j = y; j < y + len; j++)
                {
                    border[index++] = arr[x, j];
                }
                for (int i = x + 1; i < x + len; i++)
                {
                    border[index++] = arr[i, y + len - 1];
                }
                for (int j = y + len - 2; j >= y; j--)
                {
                    border[index++] = arr[x + len - 1, j];
                }
                for (int i = x + len - 2; i > x; i--)
                {
                    border[index++] = arr[i, y];
                }

                if (IsFibonacciSequence(border))
                {
                    return true;
                }

                len++;
            }

            return false;
        }

        private static bool IsFibonacciSequence(int[] arr)
        {
            if (arr.Length < 3)
            {
                return false;
            }

            for (int i = 2; i < arr.Length; i++)
            {
                if (arr[i] != arr[i - 1] + arr[i - 2])
                {
                    return false;
                }
            }

            return true;
        }
    }
}