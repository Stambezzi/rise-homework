namespace TreeTasksTests
{
    using TreeTasks;

    [TestClass]
    public class TreeTasksTests
    {
        BinaryTreeNode node35;
        BinaryTreeNode node61;
        BinaryTreeNode node76;
        BinaryTreeNode node24;
        BinaryTreeNode node38;
        BinaryTreeNode node69;
        BinaryTreeNode node11;
        BinaryTreeNode node52;
        BinaryTreeNode binaryTreeRoot;

        BinaryTreeNode NonBinarySearchTreeRoot;


        [TestInitialize]
        public void Initialize() 
        {
            node35 = new BinaryTreeNode(35, null, null);
            node61 = new BinaryTreeNode(61, null, null);
            node76 = new BinaryTreeNode(76, null, null);

            node24 = new BinaryTreeNode(24, null, null);
            node38 = new BinaryTreeNode(38, node35, null);
            node69 = new BinaryTreeNode(69, node61, node76);

            node11 = new BinaryTreeNode(11, null, node24);
            node52 = new BinaryTreeNode(52, node38, node69);

            binaryTreeRoot = new BinaryTreeNode(25, node11, node52);

            NonBinarySearchTreeRoot = new BinaryTreeNode(1, binaryTreeRoot, null);
        }

        [TestMethod]
        public void PreOrder_Returns_Right_Order()
        {
            //Arrange
            var expected = new List<int> { 25, 11, 24, 52, 38, 35, 69, 61, 76 };
            var result = new List<BinaryTreeNode>();

            //Act
            TreeTraversal.PreOrder(binaryTreeRoot, result);
            //Assert
            CollectionAssert.AreEquivalent(result.Select(x => x.Value).ToList(), expected);

        }

        [TestMethod]
        public void PostOrder_Returns_Right_Order()
        {
            //Arrange
            var expected = new List<int> { 24, 11, 35, 38, 61, 76, 69, 52, 25 };
            var result = new List<BinaryTreeNode>();

            //Act
            TreeTraversal.PostOrder(binaryTreeRoot, result);
            //Assert
            CollectionAssert.AreEquivalent(result.Select(x => x.Value).ToList(), expected);

        }

        [TestMethod]
        public void InOrder_Returns_Right_Order()
        {
            //Arrange
            var expected = new List<int> { 11, 24, 25, 35, 38, 52, 61, 69, 76 };
            var result = new List<BinaryTreeNode>();

            //Act
            TreeTraversal.PostOrder(binaryTreeRoot, result);
            //Assert
            CollectionAssert.AreEquivalent(result.Select(x => x.Value).ToList(), expected);

        }

        [TestMethod]
        public void GetHeight_Returns_Correct_Height()
        {
            //Arrange
            var expected = 3;

            //Act
            var result = TreeTraversal.GetHeight(binaryTreeRoot);

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void IsBinaryTree_With_Binary_Search_Tree_Input()
        {
            //Arrange
            var expected = true;

            //Act
            var result = TreeTraversal.IsBinarySearchTree(binaryTreeRoot);

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void IsBinaryTree_With_Non_Binary_Search_Tree_Input()
        {
            //Arrange
            var expected = false;

            //Act
            var result = TreeTraversal.IsBinarySearchTree(NonBinarySearchTreeRoot);

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void GetKthElementInTree()
        {
            //Arrange
            var expected = node24;

            //Act
            var result = TreeTraversal.GetKthSmallestElement(NonBinarySearchTreeRoot, 2);

            //Assert
            Assert.AreEqual(expected.Value, result.Value);
        }

        [TestMethod]
        public void GetKthElementInTree_With_Less_Elements_Than_K()
        {
            //Arrange
            string expected = null;

            //Act
            var result = TreeTraversal.GetKthSmallestElement(NonBinarySearchTreeRoot, 15);

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void HeapSort_With_One_Element()
        {
            //Arrange
            var input = new int[] { 1 };
            var expected = new int[] { 1 };

            //Act
            HeapSort.Sort(input);

            //Assert
            CollectionAssert.AreEquivalent(expected, input);
        }

        [TestMethod]
        public void HeapSort_With_Five_Elements()
        {
            //Arrange
            var input = new int[] { 3, 5, 1, 4, 2 };
            var expected = new int[] { 1, 2, 3, 4, 5 };

            //Act
            HeapSort.Sort(input);

            //Assert
            CollectionAssert.AreEquivalent(expected, input);
        }

        [TestMethod]
        public void HeapSort_With_Zero_Elements()
        {
            //Arrange
            var input = new int[] { };
            var expected = new int[] { };

            //Act
            HeapSort.Sort(input);

            //Assert
            CollectionAssert.AreEquivalent(expected, input);
        }
    }
}