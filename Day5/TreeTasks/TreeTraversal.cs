﻿using System.Xml.Linq;

namespace TreeTasks
{
    public class TreeTraversal
    {
        public static void PreOrder(BinaryTreeNode root, List<BinaryTreeNode> result) 
        {
            if (root != null)
            {
                result.Add(root);
                PreOrder(root.Left,result);
                PreOrder(root.Right, result);
            }
        }

        public static void PostOrder(BinaryTreeNode root, List<BinaryTreeNode> result)
        {
            if (root != null)
            {
                PreOrder(root.Left, result);
                PreOrder(root.Right, result);
                result.Add(root);
            }
        }

        public static void InOrder(BinaryTreeNode root, List<BinaryTreeNode> result)
        {
            if (root != null)
            {
                PreOrder(root.Left, result);
                result.Add(root);
                PreOrder(root.Right, result);
            }
        }

        public static int GetHeight(BinaryTreeNode root)
        {
            if (root == null || root.Left == null && root.Right == null)
            {
                return 0;
            }
            else
            {
                return 1 + Math.Max(GetHeight(root.Right), GetHeight(root.Left));
            }
        }

        private static int MaxValue(BinaryTreeNode root)
        {
            if (root == null)
            {
                return int.MinValue;
            }

            int value = root.Value;
            int leftMax = MaxValue(root.Left);
            int rightMax = MaxValue(root.Right);

            return Math.Max(value, Math.Max(leftMax, rightMax));
        }

        private static int MinValue(BinaryTreeNode root)
        {
            if (root == null)
            {
                return int.MaxValue;
            }

            int value = root.Value;
            int leftMax = MinValue(root.Left);
            int rightMax = MinValue(root.Right);

            return Math.Min(value, Math.Min(leftMax, rightMax));
        }

        public static bool IsBinarySearchTree(BinaryTreeNode root)
        {
            if (root == null)
            {
                return true;
            }
            if (root.Left != null && MaxValue(root.Left) > root.Value)
            {
                return false;
            }
            if (root.Right != null && MinValue(root.Right) < root.Value)
            {
                return false;
            }
            if (IsBinarySearchTree(root.Left) == false || IsBinarySearchTree(root.Right) == false)
            {
                return false;
            }

            return true;
        }

        public static BinaryTreeNode GetKthSmallestElement(BinaryTreeNode root, int k)
        {
            var stack = new Stack<BinaryTreeNode>();

            var count = 0;

            while (root != null || stack.Count > 0)
            {
                while (root != null)
                {
                    stack.Push(root);
                    root = root.Left;
                }

                root = stack.Pop();
                count++;

                if (count == k)
                {
                    return root;
                }

                root = root.Right;
            }

            return null;
        }
    }
}
