﻿namespace TreeTasks
{
    public class BinaryTreeNode
    {
        public int Value { get; set; }
        public BinaryTreeNode Left { get; set; }
        public BinaryTreeNode Right { get; set; }

        public BinaryTreeNode(int value, BinaryTreeNode left, BinaryTreeNode right )
        {
            this.Value = value;
            this.Left = left;
            this.Right = right;
        }
    }
}
