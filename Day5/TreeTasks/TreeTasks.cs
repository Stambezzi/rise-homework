﻿namespace TreeTasks
{
    public class TreeTasks
    {
        static void Main()
        {
            var input = new int[] { 12, 11, 13, 5, 6, 7 };
            HeapSort.Sort(input);

            Console.WriteLine("Sorted array is:");
            Console.WriteLine(string.Join(", ", input));
        }
    }
}