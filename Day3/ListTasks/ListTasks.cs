﻿using System.Collections.Generic;

namespace ListTasks
{
    public class ListTasks
    {
        public static void Main()
        {
            var input = new LinkedList<int>(new[] { 1, 2, 3, 4, 5 });
            var result = EraseMiddleElementInLinkedList(input);
            
            Console.WriteLine(result);
        }

        public static bool SolveHanoiTowers(int numDisks, Stack<int> source, Stack<int> helper, Stack<int> target,
            string sourceName, string helperName, string targetName)
        {
            var finishedSuccessfully = false;

            if (numDisks == 0)
            {
                return finishedSuccessfully;
            }

            if (numDisks == 1)
            {
                var disk = source.Pop();
                target.Push(disk);

                Console.WriteLine("Moved disk " + disk + " from " + sourceName + " to " + targetName);

                finishedSuccessfully = true;

                return finishedSuccessfully;
            }
            else
            {
                var success1 = SolveHanoiTowers(numDisks - 1, source, target, helper, sourceName, targetName, helperName);

                var disk = source.Pop();
                target.Push(disk);

                Console.WriteLine("Moved disk " + disk + " from " + sourceName + " to " + targetName);

                var success2 = SolveHanoiTowers(numDisks - 1, helper, source, target, targetName, sourceName, targetName);

                return success1 && success2;
            }
        }

        public static bool EraseMiddleElementInLinkedList(LinkedList<int> input)
        {
            if (input.Count == 0)
            {
                return false;
            }

            if (input.Count == 1)
            {
                input.RemoveFirst();

                return true;
            }

            var middleCount = (int)Math.Floor((decimal)input.Count / 2);
            var current = input.First;

            for (int i = 0; i < middleCount; i++)
            {
                current = current.Next;
            }

            input.Remove(current);

            return true;
        }

        public static List<int> GetUniqueElements(List<int> list)
        {
            var visited = new HashSet<int>();

            list.ForEach(x => visited.Add(x));

            return visited.ToList();
        }

    }
}