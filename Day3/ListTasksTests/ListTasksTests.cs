namespace ListTasksTests
{
    using ListTasks;

    [TestClass]
    public class ListTasksTests
    {
        [TestMethod]
        public void GetUniqueElements_Correctly_Returns_Unique_Elements()
        {
            //Arrange
            var input = new List<int>()
            {
                1, 2, 2, 2, 3, 3, 7
            };

            //Act
            var result = ListTasks.GetUniqueElements(input);

            //Assert
            CollectionAssert.AllItemsAreUnique(result);
        }

        [TestMethod]
        public void GetUniqueElements_Correctly_Returns_Unique_Element_Of_Array_With_Only_Unique_Elements()
        {
            //Arrange
            var input = new List<int>()
            {
                1, 1, 1, 1, 1, 1, 1
            };

            //Act
            var result = ListTasks.GetUniqueElements(input);

            //Assert
            CollectionAssert.AllItemsAreUnique(result);
        }

        [TestMethod]
        public void GetUniqueElements_Correctly_Returns_Unique_Element_Of_Array_With_One_Element()
        {
            //Arrange
            var input = new List<int>()
            {
                1
            };

            //Act
            var result = ListTasks.GetUniqueElements(input);

            //Assert
            CollectionAssert.AllItemsAreUnique(result);
        }

        [TestMethod]
        public void EraseMiddleElementInLinkedList_Of_Five_Returns_True()
        {
            //Arrange
            var input = new LinkedList<int>(new[] { 1, 2, 3, 4, 5 });
            var expected = true;

            //Act
            var result = ListTasks.EraseMiddleElementInLinkedList(input);

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void EraseMiddleElementInLinkedList_Of_Two_Returns_True()
        {
            //Arrange
            var input = new LinkedList<int>(new[] { 1, 2 });
            var expected = true;

            //Act
            var result = ListTasks.EraseMiddleElementInLinkedList(input);

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void EraseMiddleElementInLinkedList_Of_One_Returns_True()
        {
            //Arrange
            var input = new LinkedList<int>(new[] { 1 });
            var expected = true;

            //Act
            var result = ListTasks.EraseMiddleElementInLinkedList(input);

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void EraseMiddleElementInLinkedList_Of_Empty_Linked_List_Returns_False()
        {
            //Arrange
            var input = new LinkedList<int>();
            var expected = false;

            //Act
            var result = ListTasks.EraseMiddleElementInLinkedList(input);

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void EraseMiddleElementInLinkedList_Removes_Middle_Element()
        {
            //Arrange
            var input = new LinkedList<int>((new[] { 1, 2, 3 }));
            var expected = new LinkedList<int>((new[] { 1, 3 }));

            //Act
            ListTasks.EraseMiddleElementInLinkedList(input);

            //Assert
            CollectionAssert.AreEqual(input, expected);
        }

        [TestMethod]
        public void EraseMiddleElementInLinkedList_Of_Linked_List_With_Two_Middle_Elements_Removes_Right_One()
        {
            //Arrange
            var input = new LinkedList<int>((new[] { 1, 2, 3, 4 }));
            var expected = new LinkedList<int>((new[] { 1, 2, 4 })); 

            //Act
            ListTasks.EraseMiddleElementInLinkedList(input);

            //Assert
            CollectionAssert.AreEqual(input, expected);
        }

        [TestMethod]
        public void SolveHanoiTowers_With_One_Element_Returns_True()
        {
            //Arrange
            int num_disks = 1;

            var source = new Stack<int>();
            var auxiliary = new Stack<int>();
            var target = new Stack<int>();

            for (int i = num_disks; i >= 1; i--)
            {
                source.Push(i);
            }

            var expected = true;

            //Act
            var result = ListTasks.SolveHanoiTowers(num_disks, source, auxiliary, target, "Source", "Helper", "Target");

            //Assert
            Assert.AreEqual(result, expected);
        }

        [TestMethod]
        public void SolveHanoiTowers_With_Three_Element_Returns_True()
        {
            //Arrange
            int num_disks = 3;

            var source = new Stack<int>();
            var auxiliary = new Stack<int>();
            var target = new Stack<int>();

            for (int i = num_disks; i >= 1; i--)
            {
                source.Push(i);
            }

            var expected = true;

            //Act
            var result = ListTasks.SolveHanoiTowers(num_disks, source, auxiliary, target, "Source", "Helper", "Target");

            //Assert
            Assert.AreEqual(result, expected);

        }

        [TestMethod]
        public void SolveHanoiTowers_With_Zero_Element_Returns_False()
        {
            //Arrange
            int num_disks = 0;

            var source = new Stack<int>();
            var auxiliary = new Stack<int>();
            var target = new Stack<int>();

            for (int i = num_disks; i >= 1; i--)
            {
                source.Push(i);
            }

            var expected = false;

            //Act
            var result = ListTasks.SolveHanoiTowers(num_disks, source, auxiliary, target, "Source", "Helper", "Target");

            //Assert
            Assert.AreEqual(result, expected);
        }
    }
}