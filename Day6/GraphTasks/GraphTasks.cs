﻿namespace GraphTasks
{
    public class GraphTasks
    {
        static void Main()
        {
            var vertexesAndEdges = Console.ReadLine().Split(" ").Select(int.Parse).ToArray();
            var N = vertexesAndEdges[0];
            var M = vertexesAndEdges[1];

            var graph = new Graph(N);

            for (int i = 0; i < M; i++)
            {
                var xAndY = Console.ReadLine().Split(" ").Select(int.Parse).ToArray();
                var x = xAndY[0];
                var y = xAndY[1];

                graph.AddEdge(x, y);
                graph.AddEdge(y, x);
            }

            Console.WriteLine(graph.HasHamiltonianCycle());
        }
    }
}