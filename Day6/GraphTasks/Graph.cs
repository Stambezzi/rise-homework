﻿namespace GraphTasks
{
    public class Graph
    {
        private int _vertexCount;
        private List<List<int>> _matrix;
        private List<int> _path;

        public Graph(int count)
        {
            _vertexCount = count;
            _matrix = new List<List<int>>();
            for (int i = 0; i < _vertexCount; i++)
            {
                _matrix.Add(new List<int>());   
                for (int j = 0; j < _vertexCount; j++)
                {
                    _matrix[i].Add(0);
                }
            }
            _path = new List<int>();
        }

        public void AddEdge(int start, int end)
        {
            _matrix[start][end] = 1;
            _matrix[end][start] = 1;
        }

        public void DFS(int start)
        {
            var orderedVertexes = new Stack<int>();
            var visited = new HashSet<int>();

            orderedVertexes.Push(start);

            while (orderedVertexes.Count > 0)
            {
                var current = orderedVertexes.Pop();
                visited.Add(current);
                Console.WriteLine(current);

                for (int i = 0; i < _vertexCount; i++)
                {
                    if (_matrix[current][i] == 1 && !visited.Contains(i))
                    {
                        orderedVertexes.Push(i);
                    }
                }
            }
        }

        public void DFSRecursive(int vertex, HashSet<int> visited)
        {
            visited.Add(vertex);
            for (int i = 0; i < _vertexCount; i++)
            {
                if (_matrix[vertex][i] == 1 && !visited.Contains(i))
                {
                    DFSRecursive(i, visited);
                }
            }

            Console.WriteLine(vertex);
        }

        public bool HasCycle(int start)
        {
            var orderedVertexes = new Stack<int>();
            var visited = new HashSet<int>();

            orderedVertexes.Push(start);

            while (orderedVertexes.Count > 0)
            {
                var current = orderedVertexes.Pop();
                if (!visited.Add(current))
                {
                    return true;
                }

                visited.Add(current);

                for (int i = 0; i < _vertexCount; i++)
                {
                    if (_matrix[current][i] == 1 && !visited.Contains(i))
                    {
                        orderedVertexes.Push(i);
                    }
                }
            }

            return false;
        }

        public bool HasHamiltonianCycle()
        {
            _path.Clear();
            _path.Add(0);
            return HamiltonianCycleUtil(1);
        }

        private bool HamiltonianCycleUtil(int position)
        {
            if (position == _vertexCount)
            {
                if (_matrix[_path[position - 1]][_path[0]] == 1)
                {
                    return true;
                }    
                else
                {
                    return false;
                }
            }

            for (int v = 1; v < _vertexCount; v++)
            {
                if (IsSafe(v, position))
                {
                    _path.Add(v);

                    if (HamiltonianCycleUtil(position + 1))
                    {
                        return true;
                    }

                    _path.RemoveAt(_path.Count - 1);
                }
            }

            return false;
        }

        private bool IsSafe(int vertex, int position)
        {
            if (_matrix[_path[position - 1]][vertex] == 0)
            {
                return false;
            }

            for (int i = 0; i < position; i++)
            {
                if (_path[i] == vertex)
                {
                    return false;
                }
                    
            }

            return true;
        }
    }
}
