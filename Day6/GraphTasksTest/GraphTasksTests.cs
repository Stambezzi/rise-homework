namespace GraphTasksTest
{
    using GraphTasks;

    [TestClass]
    public class GraphTasksTests
    {
        [TestMethod]
        public void HasCycle_Graph_With_Cycle_Returns_True()
        {
            //Arrange
            var graph = new Graph(5);
            graph.AddEdge(0, 1);
            graph.AddEdge(0, 2);
            graph.AddEdge(0, 3);
            graph.AddEdge(3, 4);
            graph.AddEdge(2, 3);

            var expected = true;

            //Act
            var result = graph.HasCycle(0);

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void HasCycle_Graph_Without_A_Cycle_Returns_True()
        {
            //Arrange
            var graph = new Graph(5);
            graph.AddEdge(0, 1);
            graph.AddEdge(0, 2);
            graph.AddEdge(0, 3);
            graph.AddEdge(3, 4);

            var expected = false;

            //Act
            var result = graph.HasCycle(0);

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void Micro_Found_All_Coins_Or_Graph_Has_Hamiltonian_Cycle()
        {
            //Arrange
            var graph = new Graph(4);
            graph.AddEdge(0, 1);
            graph.AddEdge(1, 0);
            graph.AddEdge(1, 2);
            graph.AddEdge(2, 1);
            graph.AddEdge(2, 3);
            graph.AddEdge(3, 2);
            graph.AddEdge(3, 0);
            graph.AddEdge(0, 3);

            var expected = true;

            //Act
            var result = graph.HasHamiltonianCycle();

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void Micro_Did_Not_Find_All_Coins_Or_Graph_Does_Not_Have_A_Hamiltonian_Cycle()
        {
            //Arrange
            var graph = new Graph(6);
            graph.AddEdge(0, 1);
            graph.AddEdge(1, 0);
            graph.AddEdge(1, 2);
            graph.AddEdge(2, 1);
            graph.AddEdge(2, 3);
            graph.AddEdge(3, 2);
            graph.AddEdge(3, 4);
            graph.AddEdge(4, 3);
            graph.AddEdge(4, 1);
            graph.AddEdge(1, 4);
            graph.AddEdge(5, 3);
            graph.AddEdge(3, 5);

            var expected = false;

            //Act
            var result = graph.HasHamiltonianCycle();

            //Assert
            Assert.AreEqual(expected, result);
        }
    }
}